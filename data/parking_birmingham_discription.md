1. Source: https://archive.ics.uci.edu/ml/datasets/Parking+Birmingham

2. Information: Data collected from car parks in Birmingham that are operated by NCP from Birmingham City Council.
Occupancy rates (8:00 to 16:30) from 2016/10/04 to 2016/12/19

3. The point of the regression problem:
	To reveal the time of the measure influence value to the occupancy and the capacity influence value to the occupancy level.

4. Attribute Information:
	SystemCodeNumber: Car park ID
	Capacity: Car park capacity
	Occupancy: Car park occupancy rate
	LastUpdated: Date and Time of the measure